const mongoose = require("mongoose");
const User = require("../models/user");
const playlist=require("../samplemusic")
module.exports = {
    async registerUser(req, res) {
        try {
            let user = req.body;
            user["playlist"]=playlist;
            if (!user.email || !user.password)
                throw "Please provide User email and password";

            let newUser = new User(user);
            await newUser.save();
            res.send({ status: true, user: newUser })

        }
        catch (err) {
            res.send({ status: false, error: err })
        }
    },
    async loginUser(req, res) {
        try {
            let user = req.body;
            if (!user.email || !user.password)
                throw "Please provide User email and password";

            let dbUser = await User.findOne({ email: user.email });
            if (!dbUser) throw "User doesn't exist";
            else if (dbUser.password != user.password) throw "Password doesn't match";
            else res.send({ status: true, user: dbUser })
        }
        catch (err) {
            res.send({ status: false, error: err })
        }
    },
    async savePlaylist(req, res) {
        try {
            let user = req.body;
            if (!user.email)
                throw "Please provide User details";

            let dbUser=await User.findOneAndUpdate({email:user.email},{
                $set:{
                    playlist:user.playlist
                }
            },
            {
                new:true
            })
            res.send({status:true,user:dbUser})

        }
        catch (err) {
            res.send({ status: false, error: err })
        }
    }
}