"use strict";
const User = require("../controllers/user")
module.exports = [
    {
        method: 'POST',
        path: '/signup',
        handlers: [User.registerUser]
    }, 
    {
        method: 'POST',
        path: '/login',
        handlers: [User.loginUser]
    },
    {
        method: 'POST',
        path: '/save/playlist',
        handlers: [User.savePlaylist]
    }
]