const express=require("express");
const router=express.Router();
const routes=[
    ...require("./user")
]
routes.forEach(route=>{
    switch (route.method) {
        case 'GET':
            router.get(route.path, ...route.handlers);
            break;
        

        case 'POST':
            router.post(route.path, route.handlers);
            break;


        case 'PUT':
            router.put(route.path, route.handlers);
            break;


        case 'DELETE':
            router.delete(route.path, route.handlers);
            break;

        case 'ALL':
            router.all(route.path, route.handlers);
            break;
    };
})
module.exports=router;