const mongoose=require("mongoose");

const UserSchema=new mongoose.Schema({
    email:{
        type:String,
        default:''
    },
    name:{
        type:String,
        default:''
    },
    password:{
        type:String,
        default:''
    },
    playlist:[
        {
            name:{
                type:String,
                default:''
            },
            link:{
                type:String,
                default:''
            },
            status:{
                type:String,
                enum:['ACTIVE','DELETED'],
                default:'ACTIVE'
            }
        }
    ]
})

module.exports=mongoose.model("User",UserSchema)