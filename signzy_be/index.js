const express = require("express");
const app = express();
const mongoose = require("mongoose");
const routes=require("./routes");
const cors=require("cors");
const bodyParser=require("body-parser")
require("dotenv").config();
mongoose.connect(process.env.DB, { useNewUrlParser: true }, (err, data) => {
    if (err) console.log("error connecting DB");
    else console.log("DB connected")
})
app.use(cors());
app.use(bodyParser.json());
app.use("/",routes)
app.listen(4000, (err) => {
    if (err) console.log("error starting server");
    else console.log("server started at 4000");
})